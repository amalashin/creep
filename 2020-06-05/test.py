#!/usr/bin/venv python

import atexit
from configparser import ConfigParser
from pyVmomi import vim
from pyVim.connect import SmartConnectNoSSL, Disconnect


def check_rp_exists(parent_pool, name):
    child_rp_names = list(map(lambda rp: rp.name, parent_pool.resourcePool))
    return name in child_rp_names


def setup(root_pool):
    if check_rp_exists(root_pool, 'TKG'):
        print('Ресурс-пул с именем TKG уже существует')
        exit(0)
    pass


def main():
    # получение конфига
    config = ConfigParser()
    config.read("vcenter.cfg")
    vc = config['vcenter']

    # подключение к vCenter и регистрация дисконнекта при завершении скрипта
    connection = SmartConnectNoSSL(host=vc['host'],  user=vc['username'], pwd=vc['password'])
    atexit.register(Disconnect, connection)

    # получаем Datacenter, Cluster и корневой ресурс-пул
    content = connection.RetrieveContent()
    dc = content.rootFolder.childEntity[0]  # уровень Datacenter
    cluster = dc.hostFolder.childEntity[0]
    root_pool = cluster.resourcePool

    # выводим имена
    print(f'Datacenter: {dc.name}')
    print(f'Cluster: {cluster.name}')
    print(f'Root pool: {root_pool.name}')

    # запуск настройки инфраструктуры
    setup(root_pool)


if __name__ == '__main__':
    main()
