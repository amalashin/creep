# Ссылки, упомянутые в КРИ-П 6
- Подготовка [Instant Clones под Linux](https://docs.vmware.com/en/VMware-Horizon-7/7.12/linux-desktops-setup/GUID-92110540-8170-49C3-A150-F9C64D5075DB.html#GUID-92110540-8170-49C3-A150-F9C64D5075DB)
- Ключи и [переменные для запуска установки агента Horizon под Linux](https://docs.vmware.com/en/VMware-Horizon-7/7.12/linux-desktops-setup/GUID-09A3F97C-47FE-4ABF-B68C-E42AE26632CC.html#GUID-09A3F97C-47FE-4ABF-B68C-E42AE26632CC)
- Создание множества клонов [Instant Clones для Linux](https://docs.vmware.com/en/VMware-Horizon-7/7.12/linux-desktops-setup/GUID-9DFB6F0D-B575-4A79-996F-020736DB4A2B.html) 
- Агент [Horizon под Linux недоступен - решение проблемы с DNS](https://docs.vmware.com/en/VMware-Horizon-7/7.0/com.vmware.horizon-view.linuxdesktops.doc/GUID-127FFC83-BD23-4A91-A074-480595DFB027.html)
- Астра Линукс: Проект Орёл, [подключение репозитория Debian](https://wiki.astralinux.ru/pages/viewpage.action?pageId=3276859)
- Поиск "not supported" [текст внутри файлов в Linux рекурсивно](https://stackoverflow.com/questions/16956810/how-do-i-find-all-files-containing-specific-text-on-linux)