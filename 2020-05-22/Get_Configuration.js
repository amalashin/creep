category = Server.getConfigurationElementCategoryWithPath('VMWLAB');
elements = category.configurationElements;

elements.forEach(function(element){
  System.log('Configuration Element "' + element.name + '" has Attributes: ')
  element.attributes.forEach(function(attribute) {
    System.log('\t"' + attribute.name + '" with type "' + attribute.type + '" and value "' + attribute.value + '"')
  })
})