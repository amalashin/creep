#!/usr/bin/env python3

import json
import requests
from requests.exceptions import RequestException
import urllib3

urllib3.disable_warnings()


def handler(ctx, inputs):
    api_url = f'https://{inputs["awx_host"]}/api/v2/inventories/{inputs["inventory_id"]}/hosts/'
    headers = {'Content-Type': 'application/json', 'Authorization': f'Bearer {inputs["token"]}'}
    data = json.dumps({"name": inputs['new_host'], "description": inputs['new_host_info'], "enabled": True})

    try:
        resp = requests.post(api_url, headers=headers, data=data, verify=False)
        content = resp.content
        print(f'Add Host: request finished with status code {resp.status_code}')
    except RequestException as e:
        raise SystemExit(e)

    host = json.loads(content)
    if 'id' in host:
        print(f'Add Host: New host ID = {host["id"]}')
        return host['id']
    else:
        raise SystemExit(host)


if __name__ == '__main__':
    handler('', {
        "awx_host": "<AWX FQDN OR IP HERE>",
        "token": "<YOUR TOKEN HERE>",
        "inventory_id": 0,
        "new_host": 'a.b.c.d',
        "new_host_info": "Test host added via API"
    })
