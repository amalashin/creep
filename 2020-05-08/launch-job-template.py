#!/usr/bin/env python3

import json
import requests
from requests.exceptions import RequestException
import urllib3

urllib3.disable_warnings()


def handler(ctx, inputs):
    api_url = f'https://{inputs["awx_host"]}/api/v2/job_templates/{inputs["job_template_id"]}/launch/'
    headers = {'Content-Type': 'application/json', 'Authorization': f'Bearer {inputs["token"]}'}

    try:
        resp = requests.post(api_url, headers=headers, verify=False)
        content = resp.content
        print(f'Launch Job Template: request finished with status code {resp.status_code}')
    except RequestException as e:
        raise SystemExit(e)

    launch = json.loads(content)
    if 'id' in launch:
        print(f'Launch Job Template: Job ID = {launch["id"]}')
        return launch['id']
    else:
        raise SystemExit(launch)


if __name__ == '__main__':
    handler('', {
        "awx_host": "<AWX FQDN OR IP HERE>",
        "token": "<YOUR TOKEN HERE>",
        "job_template_id": 0
    })
