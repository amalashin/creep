#!/usr/bin/env python3

import json
import requests
from requests.exceptions import RequestException
import urllib3

urllib3.disable_warnings()


def handler(ctx, inputs):
    api_url = f'https://{inputs["awx_host"]}/api/v2/inventories/'
    headers = {'Content-Type': 'application/json', 'Authorization': f'Bearer {inputs["token"]}'}

    try:
        resp = requests.get(api_url, headers=headers, verify=False)
        content = resp.content
        print(f'List Inventories: request finished with status code {resp.status_code}')
    except RequestException as e:
        raise SystemExit(e)

    return json.loads(content)


if __name__ == '__main__':
    handler('', {
        "awx_host": "<AWX FQDN OR IP HERE>",
        "token": "<YOUR TOKEN HERE>",
    })
