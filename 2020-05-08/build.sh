#!/bin/zsh

rm -rf actions/*.zip
for f in *.py
do
    zip -r -X actions/"${f%.*}" lib/ $f
done
